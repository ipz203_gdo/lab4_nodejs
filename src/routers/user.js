const express = require("express");
const Router = express.Router();

const User = require('../models/user');
const auth = require("../middlewares/auth");

// LOGIN
Router.post("/users/login", async (req, res) => {
  try{
    const user = await User.findOneByCredentials(req.body.email, req.body.password);
    const token = await user.generateAuthToken();

    res.send({user, token});
  }
  catch (e) {
    res.status(400).send(e.message);
  }
});

// ME
Router.get("/users/me", auth, async (req, res) => {
  const user = await User.findOne({_id: req.user.id})
  const tasks = await user.populate('tasks')
  res.send(tasks);
  
});

// UPDATE
Router.put("/users/update", auth, async (req, res) => {
  try {
    let user = req.user;

    if (!user) {
      res.status(404).send(`User with id "${req.params.id}" not found`);
    } else {
      user.email = req.body.email;
      user.age = req.body.age;
      user.name = req.body.name;
      user.password = req.body.password;

      const token = await user.generateAuthToken();
      await user.save()

      res.status(200).send({user, token});
    }
  } catch (e) {
    res.status(400).send(e.message);
  }
});

// REGISTRATION
Router.post("/users/register", async (req, res) => {
  try {
    const user = new User({
      email: req.body.email,
      password: req.body.password,
      age: req.body.age,
      name: req.body.name
    });

    const token = await user.generateAuthToken();

    await user.save();

    res.status(200).send({user,token});
  } catch (e) {
    res.status(400).send(e.message);
  }
});

// LOGOUT
Router.post("/users/logout", auth, async (req, res) => {
  try {
    req.user.tokens = req.user.tokens.filter(token => {
      return token.token !== req.token;
    });

    await req.user.save();
    res.send('Logout success!');
  } catch (e) {
    //res.send(`${req.user.tokens}, `);
    res.status(500).send();
  }
});

// DELETE ONE
Router.delete("/users/delete", auth, async (req, res) => {
    try{
      await User.findByIdAndDelete(req.user.id)
      res.status(200).send('Delete success')
  }catch (error){
      res.status(500).send(error.message)
  }
});


//--------------------------------------------------------------------------------------------------------------------
Router.route('/users')
    .get(function (req, res)
    {
      User.find({}).then((users)=>{
        res.status(200).send(users);
      })
          .catch((error)=>{
            res.status(500).send();
          })
    });




Router.route('/users/:id')
    .get(function (req, res)
    {
      //626f991bdf1fe46f73dd4e51
      User.find({_id: req.params.id}).then((users)=>{
        res.status(200).send(users);
      })
          .catch((error)=>{
            res.status(500).send();
          })
    })
    .delete(async (req, res) =>
    {
      try {
        await User.findOneAndRemove({_id: req.params.id}).then((e)=>{
          res.status(200).send(e);
        })

      } catch (e) {
        res.status(400).send(e.message);
      }
    })
    .put(async (req, res) => {
      try {

        const user = await User.findById(req.params.id);
        const updates = ['name', 'email', 'password', 'age'];
        updates.forEach((update) => user[update] = req.body[update]);

        await user.save();
        res.status(200).send(user);

      } catch (e) {
        res.status(500).send(e.message);
      }
    });


module.exports = Router;