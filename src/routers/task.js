const Task = require("../models/task");
const express = require('express');
const auth = require("../middlewares/auth");

const Router = new express.Router();

Router.route('/tasks')
    .get( auth,async(req, res)=>
    {
      try{
        const user = req.user
        await user.populate('tasks')
        const tasks = user.tasks
        await Task.find({})
        res.status(200).send(tasks)
    }catch (error){
        res.status(500).send(error.message)
    }
    })
    .post(auth, async (req, res) => {
      const task = new Task({
        ...req.body,
        owner: req.user.id
    })
    try{
        await task.save()
        res.status(201).send(task)
        console.log(task)
    }catch (error){
        res.status(500).send(error.message)
    }
    });




Router.route('/tasks/:id')
    .get(auth,async (req, res)=>
    {
      await Task.find({_id: req.params.id}).then((tasks)=>{
        if(tasks.length===0){
          return res.status(404).send("Task not found");
        }
        res.status(200).send(tasks);
      })
          .catch((e)=>{
            res.status(500).send(e.message);
          })
    })
    .delete(auth,async (req, res) =>
    {
      try {
        let user_id = req.user.id
        let task = await Task.findById(req.params.id)
        await task.populate('owner')
        if(task.owner.id === user_id){
            task.remove()
            res.status(200).send('Task delete')
        }
        else{
            res.status(404).send("You can't to do it")
        }

      } catch (e) {
        res.status(400).send(e.message);
      }
    })
    .put(auth,async (req, res) => {
      try {
        let task = await Task.findById(req.params.id)
        await task.populate('owner')
        if(task.owner.id === req.user.id){
            task.description = req.body.description
            task.completed = req.body.completed
            await task.save()
            res.status(200).send(task)
        }
        else{
            res.status(404).send("You can't to do it")
        }

      } catch (e) {
        res.status(500).send(e.message);
      }
    });



module.exports = Router