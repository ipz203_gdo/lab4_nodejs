const {MONGOL} = require('../config');

const mongoose = require('mongoose');

const connectDB =  async () => {
  try {
    await mongoose.connect(MONGOL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
  } catch (e) {
    throw e;
  }
}

module.exports = connectDB;