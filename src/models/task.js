const mongoose = require("mongoose");
const validator = require("validator");

let Task = mongoose.model('Task', {
  description:
      {
        type: String,
        required: true,
        trim: true,

      },

  completed:
      {
        type: String,
        required: false,
        default: false
      },
      owner:
      {
          type: mongoose.Schema.Types.ObjectId,
          required: true,
          ref: "User"
      }
});




module.exports = Task